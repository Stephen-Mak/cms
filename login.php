
<?php $title = 'Login Page';?>
<?php include 'templates/header.php'; ?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container text-center">
        <h2>Online Book Store</h2>
        <p>We sell newly released Books</p>
      </div>
    </div>

    <div class="container text-justify">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12">
          <h2>This is our Login page</h2>
          <form>
            <div class="form-group">
              <label for="input-name">User Name</label>
              <input id="input-name" type="text" class="form-control" placeholder="Username">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <button type="submit" class="btn btn-default" name="submit">Submit</button>
          </form>
        </div>
      </div>

      <hr>
<?php include 'templates/footer.php';


